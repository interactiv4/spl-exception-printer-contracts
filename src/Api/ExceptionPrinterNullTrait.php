<?php
declare(strict_types=1);
/**
 * @author Interactiv4 Team
 * @copyright Copyright © Interactiv4 (https://www.interactiv4.com)
 */

namespace Interactiv4\Contracts\SPL\Exception\Printer\Api;

use Throwable;

/**
 * Trait ExceptionPrinterNullTrait.
 *
 * Use this trait to help yourself to implement ExceptionPrinterInterface.
 * It swallows the exception without printing anything.
 *
 * @see ExceptionPrinterInterface
 *
 * @api
 *
 * @package Interactiv4\Contracts\SPL\Exception\Printer
 */
trait ExceptionPrinterNullTrait
{
    /**
     * {@inheritdoc}
     */
    public function printException(Throwable $throwable): void
    {
        // noop;
    }
}
