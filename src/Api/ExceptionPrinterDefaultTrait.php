<?php
declare(strict_types=1);
/**
 * @author Interactiv4 Team
 * @copyright Copyright © Interactiv4 (https://www.interactiv4.com)
 */

namespace Interactiv4\Contracts\SPL\Exception\Printer\Api;

use Throwable;

/**
 * Trait ExceptionPrinterDefaultTrait.
 *
 * Use this trait to help yourself to implement ExceptionPrinterInterface.
 * It prints a exception message and their previous exceptions ones to stdout.
 *
 * @see ExceptionPrinterInterface
 *
 * @api
 *
 * @package Interactiv4\Contracts\SPL\Exception\Printer
 */
trait ExceptionPrinterDefaultTrait
{
    /**
     * {@inheritdoc}
     */
    public function printException(Throwable $throwable): void
    {
        $printException = static function (Throwable $throwable, bool $isPrevious = false): void {
            echo \sprintf(
                '%s Exception %s: %s' . \str_repeat(PHP_EOL, 2),
                $isPrevious ? 'Previous' : 'Main',
                \get_class($throwable),
                $throwable->getMessage()
            );
        };

        $printException($throwable, false);

        while ($throwable = $throwable->getPrevious()) {
            $printException($throwable, true);
        }
    }
}
