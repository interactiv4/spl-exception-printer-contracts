<?php
declare(strict_types=1);
/**
 * @author Interactiv4 Team
 * @copyright Copyright © Interactiv4 (https://www.interactiv4.com)
 */

namespace Interactiv4\Contracts\SPL\Exception\Printer\Api;

use Throwable;

/**
 * Interface ExceptionPrinterInterface.
 *
 * Print an exception.
 *
 * @see ExceptionPrinterDefaultTrait
 * @see ExceptionPrinterNullTrait
 *
 * @api
 *
 * @package Interactiv4\Contracts\SPL\Exception\Printer
 */
interface ExceptionPrinterInterface
{
    /**
     * Print supplied exception message and its previous exceptions ones.
     *
     * @param Throwable $throwable
     *
     * @return void
     */
    public function printException(Throwable $throwable): void;
}
